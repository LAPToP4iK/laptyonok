//
//  String+Ex.swift
//  Laptyonok_Task1_Clevertec
//
//  Created by Nikita Laptyonok on 06.09.2021.
//

import Foundation

extension String {
    var localized: String {
        let path = Bundle.main.path(forResource: LanguageManager.shared.currentLanguage.code, ofType: "lproj")
    let bundle = Bundle(path: path!)

    return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
