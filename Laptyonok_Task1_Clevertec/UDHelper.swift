//
//  UDHelper.swift
//  Laptyonok_Task1_Clevertec
//
//  Created by Nikita Laptyonok on 07.09.2021.
//

import Foundation

class UDHelper {
    static let userDefaults = UserDefaults.standard
    
    enum Key: String {
        case appearance
        case languageCode
    }
    
    private static func set<T>(_ value: T, forKey key: Key) {
        userDefaults.setValue(value, forKey: key.rawValue)
        print("Set value for key: \(key.rawValue), value: \(value)")
    }
    
    private static func get<T>(forKey key: Key) -> T? {
        let value = userDefaults.value(forKey: key.rawValue)
        print("Get value for key: \(key.rawValue), value: \(value ?? "")")
        return value as? T
    }
}

extension UDHelper {
    static var appearance: Int {
        get { return get(forKey: .appearance) ?? 0 }
        set { set(newValue, forKey: .appearance) }
    }
    
    static var language: Language {
        get { return Language(rawValue: get(forKey: .languageCode) ?? "en") ?? .english }
        set { set(newValue.code, forKey: .languageCode) }
    }
}
