//
//  ViewController.swift
//  Laptyonok_Task1_Clevertec
//
//  Created by Nikita Laptyonok on 05.09.2021.
//

import UIKit

class ViewController: UIViewController {
    
    private let languageModel = Language.allCases
    
    // MARK: - Properties
    private let logoImage: UIImageView = {
        let iv = UIImageView()
        let image = #imageLiteral(resourceName: "groups")
        iv.tintColor = .label
        iv.image = image
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    private let greetingLabel: UILabel = {
       let label = UILabel()
        label.text = "login.title".localized
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 33)
        return label
    }()
    
    private let languagePickerView: UIPickerView = {
       let pv = UIPickerView()
        return pv
    }()
    
    private lazy var systemApperanceButton: UIButton = {
        let button = createButton(withTitle: "system.theme.appereance".localized, tag: 0, andColor: .label)
        button.addTarget(self, action: #selector(changeAppearancePressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var lightApperanceButton: UIButton = {
        let button = createButton(withTitle: "light.theme.appereance".localized, tag: 1, andColor: .label)
        button.addTarget(self, action: #selector(changeAppearancePressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var darkApperanceButton: UIButton = {
        let button = createButton(withTitle: "dark.theme.appereance".localized, tag: 2, andColor: .label)
        button.addTarget(self, action: #selector(changeAppearancePressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var appearanceButtons = [systemApperanceButton, lightApperanceButton, darkApperanceButton]
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languagePickerView.delegate = self
        languagePickerView.dataSource = self
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let index = languageModel.firstIndex(of: UDHelper.language) ?? 0
        
        languagePickerView.selectRow(index, inComponent: 0, animated: true)
    }
    
    // MARK: - Actions
    @objc func changeAppearancePressed(sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.appearanceButtons.forEach { $0.alpha = 1 }
        }
        switch sender.tag {
        case 0:
            view.window?.overrideUserInterfaceStyle = .unspecified
        case 1:
            view.window?.overrideUserInterfaceStyle = .light
        case 2:
            view.window?.overrideUserInterfaceStyle = .dark
        default:
            print("unknown tag")
        }
        UIView.animate(withDuration: 0.3) {
            sender.alpha = 0.5
        }
        UDHelper.appearance = sender.tag
    }
    
    // MARK: - Helpers
    private func configureUI() {
        view.backgroundColor = .systemBackground

        view.addSubview(logoImage)
        logoImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             logoImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
             logoImage.widthAnchor.constraint(equalToConstant: 100),
             logoImage.heightAnchor.constraint(equalToConstant: 100)])
        
        view.addSubview(greetingLabel)
        greetingLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [greetingLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             greetingLabel.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: 20)])
        
        let stack = UIStackView(arrangedSubviews: [systemApperanceButton, lightApperanceButton, darkApperanceButton])
        stack.spacing = 20
        stack.distribution = .fillEqually
        view.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [stack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             stack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
             stack.topAnchor.constraint(equalTo: greetingLabel.bottomAnchor, constant: 20)])
        
        view.addSubview(languagePickerView)
        languagePickerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [languagePickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             languagePickerView.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 20),
             languagePickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16)])
    }
    
    private func localizeViews() {
        greetingLabel.text = "login.title".localized
        systemApperanceButton.setTitle("system.theme.appereance".localized, for: .normal)
        darkApperanceButton.setTitle("dark.theme.appereance".localized, for: .normal)
        lightApperanceButton.setTitle("light.theme.appereance".localized, for: .normal)
    }
    
    private func createButton(withTitle title: String, tag: Int, andColor color: UIColor) -> UIButton {
        let button = UIButton()
        let appearanceIndex: Int = UDHelper.appearance
        
        button.tag = tag
        button.alpha = tag == appearanceIndex ? 0.5 : 1
        button.setTitle(title, for: .normal)
        button.setTitleColor(.systemBackground, for: .normal)
        button.backgroundColor = color
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.layer.cornerRadius = 8
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        return button
    }
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        languageModel.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languageModel[row].title.localized
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        Language.selected = languageModel[row]
        localizeViews()
        pickerView.reloadAllComponents()
    }
}


