//
//  Language.swift
//  Laptyonok_Task1_Clevertec
//
//  Created by Nikita Laptyonok on 07.09.2021.
//

import Foundation

class LanguageManager {
    static let shared = LanguageManager()
    private init() {}
    
    var currentLanguage = UDHelper.language
}

enum Language: String, CaseIterable {
    case english = "en"
    case russian = "ru"
    case belarusian = "be"

    var code: String {
        return self.rawValue
    }
    
    var title: String {
        return "\(code).title".localized
    }
    
    var index: Int {
        return Language.allCases.firstIndex(of: self) ?? 0
    }

    static var selected: Language {
        set {
            LanguageManager.shared.currentLanguage = newValue
        }
        get {
            return UDHelper.language
        }
    }
}
